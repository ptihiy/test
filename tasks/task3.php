<p>Реализация SeekableIterator для текстового файла в 2Гб не представляется мне сколько-нибудь эффективным и оправданным решением, так как для использования функции seek нам необходимо знать битовые смещения всех строк, т.е. сначала пройти весь файл и посчитать их, и хранить их массив, который может сам занять огромное пространство в памяти.</p>
<pre>
<code>
use SeekableIterator;

class TestSeekableIterator implements SeekableIterator {

    private $handle;
    private $position = 0;
    private $stringOffsets = [];

    /* Methods required for Iterator interface */

    public function __construct($path) {
        $this->handle = fopen($path, 'r');

        // Создаем массив смещений (p.s. он может быть очень большим)
        $this->calculateStringOffsets();
    }

    private function calculateStringOffsets() {

        // Позиция первой строки всегда 0
        $this->stringOffsets[] = 0;

        while (fgets($this->handle)) {
            $this->stringOffsets[] = ftell($this->handle);
        }
    }

    public function rewind() {
        rewind($this->handle);
    }

    public function current() {
        fseek($this->stringOffsets[$this->position]);
        return fgets($this->handle);
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function seek($position) {
        $this->position = $position;

        if (!$this->valid()) {
            throw new OutOfBoundsException("invalid seek position ($position)");
        }
    }

    public function valid() {
        return isset($this->stringOffsets[$this->position]);
    }
}
</code>
</pre>