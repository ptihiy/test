<table class="table">
    <thead>
    <tr>
        <th scope="col" data-sortable data-type="integer">#</th>
        <th scope="col" data-sortable data-type="date">Дата</th>
        <th scope="col" data-sortable data-type="text">Имя</th>
        <th scope="col" data-sortable data-type="decimal">Сумма</th>
    </tr>
    </thead>
    <tbody id="task1data">
    </tbody>
</table>

<script>
    $(function() {
        var initTable = data => {
            $('#task1data').empty();
            $.each(data, (key, val) => {
                $('<tr>' +
                    '<td>' + val["#"] + '</td>'
                    + '<td>' + val["Дата"] + '</td>'
                    + '<td>' + val["Имя"] + '</td>'
                    + '<td>' + val["Сумма"] + '</td>'
                    + '</tr>').appendTo('#task1data');
            });
        }

        fetch('task1.json')
            .then(response => response.json())
            .then(data => {
                initTable(data);
            });

        $('[data-sortable]').click(function() {
            var $this = $(this);
            var type = $this.data('type');
            var index = $this.index();

            // Сортировать прямо или в обратном порядке?
            var isDirectOrder = $this.is('[data-direct]');
            console.log(isDirectOrder)
            if (isDirectOrder) {
                $('[data-sortable]').attr('data-direct', null);
            } else {
                $this.attr('data-direct', '');
            }

            var integerSort = (a, b) => a - b;
            var dateSort = (a, b) => {
                var A = a.replace(/(\d{2})\.(\d{2})\.(\d{4})/, "$3.$2.$1");
                var B = b.replace(/(\d{2})\.(\d{2})\.(\d{4})/, "$3.$2.$1");
                if (A < B) {
                    return -1;
                }
                if (A > B) {
                    return 1;
                }
                return 0;
            }
            var textSort = (a, b) => {
                var A = a.toUpperCase();
                var B = b.toUpperCase();
                if (A < B) {
                    return -1;
                }
                if (A > B) {
                    return 1;
                }
                return 0;
            }
            var decimalSort = (a, b) => a - b;

            var getSortingFunction = type => {
                switch(type) {
                    case 'integer': return integerSort;
                    case 'date': return dateSort;
                    case 'text': return textSort;
                    case 'decimal': return decimalSort;
                }
            }

            var $items = $('#task1data').children();

            var $sortedItems = $items.sort((rowA, rowB) => {
                var a = $(rowA).find('td:eq(' + index + ')').html();
                var b = $(rowB).find('td:eq(' + index + ')').html();
                var result =  getSortingFunction(type)(a, b);
                return isDirectOrder ? result * (-1) : result;
            });

            $('#task1data').empty().append($sortedItems);
        });
    });
</script>