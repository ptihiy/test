<form id="feedback">
    <div class="form-group">
        <label for="name">Представьтесь</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Ваше имя" data-validate-required data-validate-min-length="3">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Ваш email" data-validate-required data-validate-email>
        <small id="emailHelp" class="form-text text-muted">Мы не распространяем данные пользователей.</small>
    </div>
    <div class="form-group">
        <label for="comment">Комментарий</label>
        <textarea class="form-control" id="comment" name="comment" rows="3" data-validate-required></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Отправить</button>
</form>

<script>
    // Заготовка для плагина
    var validate = form => {
        // Убираем старые сообщения об ошибках
        $(form).find('.errors-group').remove();

        // Пока что все идет хорошо...
        var formIsValid = true;

        // Сюда можно добавить любые валидации
        var validations = {
            required: el => {
                var isValid = el.value.length > 0;
                return {
                    'isValid': isValid,
                    'error': "Поле обязательно для заполнения"
                }
            },
            email: el => {
                var isValid = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(el.value);
                return {
                    'isValid': isValid,
                    'error': "Такой email недопустим"
                }
            },
            "min-length": el => {
                var isValid = el.value.length >= 3;
                return {
                    'isValid': isValid,
                    'error': "Строка слишком короткая"
                }
            }
        }

        $.each(form.elements, function(key, value) {
            var isValid = true;
            var errors = [];
            var el = this;
            $(this.attributes).each(function() {
                if (this.name.startsWith('data-validate-')) {
                    var validation = this.name.substring('data-validate-'.length);
                    if (validation in validations) {
                        var processed = validations[validation](el);
                        if (!processed.isValid) {
                            isValid = false;
                            errors.push(processed.error)
                        }
                    }
                }
            });

            if (errors.length) {
                formIsValid = false;
                var $errors = $.map(errors, function(error) {
                    return $('<small class="form-text form-error"></small>').text(error);
                });
                $(el).closest('.form-group').append(($('<div class="errors-group"></div>').append($errors)));
            }
        });

        return formIsValid;
    }

    $(function() {
        $('#feedback').submit(function() {
            var $this = $(this);

            if (validate(this)) {
                $.post('/server.php', $this.serialize(), function(data) {
                    if (!data || data.result !== true) {
                        console.log('Ошибка');
                    }
                }).fail(function() {
                    console.log('Что-то не в порядке с сетью!');
                });
            }

            return false;
        });
    });
</script>