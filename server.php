<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    // Валидация JS и PHP должна быть сквозная, если форма хранится в БД, это элементарно
    // Но писать второй раз тот же самый валидатор в тестовом задании скучно
    // Поэтому он как бы тут есть

    $result = true;

    try {
        $db = new \PDO('mysql:host=localhost;dbname=test;charset=utf8mb4', 'test', 'test');

        $stmt = $db->prepare("INSERT INTO feedbacks(`name`, `email`, `comment`) VALUES(:name,:email,:comment)");
        $stmt->execute(array(':name' => $_POST['name'], ':email' => $_POST['email'], ':comment' => $_POST['comment']));
        $affected_rows = $stmt->rowCount();

        if (!$affected_rows) {
            // WTF?
            $result = false;
        } else {
            // Отправляем почту, СМСки и т.п. менеджерам
        }
    } catch(\PDOException $e) {
        // Логируем, уведомляем администратора по Bugsnag или т.п.
        $result = false;
    }


    echo json_encode(['result' => $result]);
}
die();
?>