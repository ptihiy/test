<?php

use SeekableIterator;

class TestSeekableIterator implements SeekableIterator {

    private $handle;
    private $position = 0;

    private $array = array(
        "first element",
        "second element",
        "third element",
        "fourth element"
    );

    public function seek($position) {
        $this->position = $position;

        if (!$this->valid()) {
            throw new OutOfBoundsException("invalid seek position ($position)");
        }
    }

    /* Methods required for Iterator interface */

    public function __construct($path) {
        $this->handle = fopen($path, 'r');
    }

    public function rewind() {
        rewind($this->handle);
    }

    public function current() {
        return $this->array[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->array[$this->position]);
    }
}